#!/usr/bin/env sh

set -e -u -x
set -o pipefail

cd src

VERSION=$(cat .git/ref)

echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER successfully built" >> ../message/passed
echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER failed building" >> ../message/failed
echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER was aborted while building" >> ../message/aborted

pyo3-pack build -i $PYTHON_INTERPRETER

cd ..
cp -R src/target/wheels/* wheels