#!/usr/bin/env sh

set -e -u -x
set -o pipefail

cd src

VERSION=$(cat .git/ref)

echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER successfully uploaded " >> ../message/pypi_passed
echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER failed uploading" >> ../message/pypi_failed
echo "rust-markdown-text-puller $VERSION - manylinux $PYTHON_INTERPRETER was aborted while uploading" >> ../message/pypi_aborted