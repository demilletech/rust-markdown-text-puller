use pulldown_cmark::{Event, Parser};
use pyo3::prelude::*;

#[pymodule]
fn markdown_text_puller(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(pyo3::wrap_pyfunction!(get_raw_text))?;
    Ok(())
}

#[cfg(test)]
#[test]
fn test() {
    println!("{}", get_raw_text("# Hello World".to_string()).unwrap());
    println!(
        "{}",
        get_raw_text("[text](https://url.com)".to_string()).unwrap()
    );
}

#[pyfunction]
fn get_raw_text(md: String) -> PyResult<String> {
    let parser = Parser::new(&md);
    let mut output_texts: Vec<String> = Vec::new();
    for event in parser {
        match event {
            Event::Text(text) => output_texts.push(text.to_string()),
            _ => continue,
        }
    }
    Ok(output_texts.join(" "))
}
