# The stupid simple rust markdown text puller

Basically, this has one function and one function alone. It takes all the human readable text from Markdown, and extracts it. That's it.

## Why does this library exist?

I needed it for a project, and pip was the easiest place to put it.

## What can I do with this library?

Literally anything. I'm not fussed. It's ten lines of code.

## Installation

```python
pip install rust-markdown-text-puller
```

## Usage

```python
from rust_markdown_text_puller import get_raw_text

print(get_raw_text("# Raw Markdown!"))
```

It really is that simple

## License

MIT license. See the LICENSE file for details
